(function ($) {
  $( document ).on( "wb-ready.wb", function( event ) {

    var request = new XMLHttpRequest();
    var currentDate = new Date();
    var language = (wb.lang === "en") ? "eng" : "fra";

    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', Drupal.settings.ajaxLightboxEndpoint, true);
    request.onload = function () {

      // Begin accessing JSON data here
      var data = JSON.parse(this.response);
      var startDate = new Date(data.start_date);
      var endDate = new Date(data.end_date);
      var cookieExpireDate = new Date(data.cookie_expire_date);

      launchSurvey(startDate, endDate, cookieExpireDate, language);
    };

    request.send();
  });

  /**
   * Returns the value of a specific cookie, or undefined.
   */
  getCookie = function(name) {
    var cookies = document.cookie.split(";"),
    len = cookies.length,
    i,
    value='',
    cpair;

    for (i = 0; i < len; i += 1) {
      cpair = cookies[i].split("=");

      if ($.trim( cpair[0] ) === name) {
        value = cpair[1];
        break;
      }
    }

    return value;
  };

  /**
   * Sets a cookie (name, value and expiration in days).
   */
  setCookie = function(name, value, end) {
    var expires = "",
    date;

    if (end !== undefined) {
      switch (end.constructor) {
        case Number:
          date = new Date();
        date.setTime(date.getTime() + (end * 24 * 60 * 60 * 1000));
        break;
        case String:
          date = new Date(end);
        break;
      }

      expires = "expires=" + date.toGMTString();
    }

    // Set the cookie domain.
    var cookieDomain = Drupal.settings.ajaxLightboxCookieDomain;
    if (typeof(cookieDomain) == "undefined" || cookieDomain == null) {
      cookieDomain = window.location.hostname;
    }

    document.cookie = name + "=" + value + ";" + expires + "; path=/;domain=" + cookieDomain;
  };

  function launchSurvey(startDate, endDate, cookieExpireDate, language) {
    cookieName = Drupal.settings.ajaxLightboxCookieName;
    persCookieName = cookieName + "-persistent";
    sessCookieName = cookieName + "-session";

    // Proceed if the current page isn't a splash/welcome page.
    if (!$("body").hasClass("splash")) {
      theCookie = getCookie(persCookieName);

      // Proceed if the user's browser doesn't already have a survey cookie.
      if (!(theCookie)) {
        expire = cookieExpireDate.toGMTString();
        setCookie(persCookieName, "ignored", expire);

        $(document).trigger("open.wb-lbx", [
                            [
                              {
          src: language + "/ajax/lightbox",
          type: "ajax"
        }
        ],
        false,
        ]);

        // Close the lightbox popup and set the cookie.
        $(document).on('click', '.popup-lightbox-yes', function() {
          $('.mfp-close').trigger('click');
          setCookie(persCookieName, 'accepted', expire);
        });

        // Set the cookie.
        $(document).on('click', '.popup-lightbox-no', function() {
          setCookie(persCookieName, 'declined', expire)
        });
      }
    }
  };
})(jQuery);
