<?php

/**
 * @file
 * Ajax Lightbox Client module.
 */

/**
 * Implements hook_init().
 */
function ajax_lightbox_client_init() {
  drupal_add_js(array(
    'ajaxLightboxEndpoint' => variable_get('ajax_lightbox_client_endpoint'),
    'ajaxLightboxCookieName' => variable_get('ajax_lightbox_client_cookie_name'),
    'ajaxLightboxCookieDomain' => variable_get('ajax_lightbox_client_cookie_domain'),
  ), 'setting');
}

/**
 * Implements hook_menu().
 */
function ajax_lightbox_client_menu() {
  $items = array();

  $items['admin/config/content/ajax_lightbox_client'] = array( 
    'title' => 'Ajax Lightbox Client',
    'description' => 'Configuration for Ajax Lightbox Client.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['ajax_lightbox_client_admin'],
    'type' => MENU_NORMAL_ITEM,
    'access arguments' => array('access content')
  );

  return $items;
}

/**
 * Admin form callback.
 */
function ajax_lightbox_client_admin($form, &$form_state) {
  $form = array();

  $form['ajax_lightbox_client'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ajax Lightbox Client Settings'),
  );
  $form['ajax_lightbox_client']['ajax_lightbox_client_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Ajax lightbox host endpoint'),
    '#default_value' => variable_get('ajax_lightbox_client_endpoint'),
    '#description' => t('The .json endpoint of the host for ajax_lightbox. Check the <em>Ajax Lightbox Endpoint</em> section of your <a href="@ajax_lightbox">ajax_popup settings.</a>', array('@ajax_lightbox' => 'admin/config/content/ajax_lightbox')),
  );
  $form['ajax_lightbox_client']['ajax_lightbox_client_cookie_domain'] = array( 
    '#type' => 'textfield',
    '#title' => t('Domain for cookie'),
    '#default_value' => variable_get('ajax_lightbox_client_cookie_domain'),
    '#description' => t('The domain for the cookie to store on the visitor\'s computer. If you\'re unsure, you can safely leave this blank.'),
  );
  $form['ajax_lightbox_client']['ajax_lightbox_client_cookie_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of cookie'),
    '#default_value' => variable_get('ajax_lightbox_client_cookie_name', 'ajax-lightbox-popup'),
    '#description' => t('The name of the cookie to store on the visitor\'s computer.'),
  );

  return system_settings_form($form);
}

/**
 * Implements template_preprocess().
 */
function ajax_lightbox_client_preprocess(&$vars, $hook) {

  $endpoint = variable_get('ajax_lightbox_client_endpoint');
  $data = array(
    'external' => $endpoint,
  );

  $options = array(
    'type' => 'external',
    'every_page' => TRUE,
  );

  drupal_add_js($endpoint, $options);
}
