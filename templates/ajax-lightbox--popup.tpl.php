<?php
/**
 * @file
 * Default theme implementation for Lightbox.
 *
 * Available variables:
 * - $bean: A bean object holding the bean to be displayed inside of the
 *   popup lightbox.
 * - $yes: The text for the 'yes' button
 * - $no: The text for the 'no' button
 * - $url: The URL to go to if the user clicks on yes button.
 */
?>

<section id="popup" class="wb-lbx modal-dialog modal-content overlay-def" >
  <header class="modal-header"><h2 class="modal-title"><?php print $bean->title ?></h2></header>
  <div class="modal-body">
    <?php print drupal_render(bean_view($bean)); ?>
  </div>
  <div class="modal-footer">
<?php print $title ?>
    <a href="<?php print $url ?>" target="_blank" class="btn btn-primary btn-md popup-lightbox-yes" type="button"><?php print $yes ?></a>
    <button class="btn btn-default btn-md popup-modal-dismiss popup-lightbox-no" type="button"><?php print $no ?></button>
  </div>
  <button title="Close overlay (escape key)" type="button" class="mfp-close">×<span class="wb-inv"> Close overlay (escape key)</span></button>
</section>
