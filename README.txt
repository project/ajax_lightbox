CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------

The Ajax Lightbox module is devided into server and client modules. The server
allows you to configure the content, date range and cookie expiration for your
desired lightbox content.

The client allows you to point to an endpoint either hosted locally or remotely,
and load that popup according to the settings configured on the server.

For a full description of the project visit the project page:
http://drupal.org/project/ajax_lightbox

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/ajax_lightbox


REQUIREMENTS
------------

Wetkit - (https://www.drupal.org/project/wetkit)


INSTALLATION
------------

Install as you would normally install a contributed Drupal. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

CONFIGURATION
-------------

 * Configure Ajax Lightbox Server settings in Administration » Configuration »
   Content Authoring » Ajax Lightbox:

   Set the bean that you want loaded, the url to trigger for users that click
   on the yes button, your date range for the popup, and the expiry date for
   the cookie in case you'd like to show the popup a second time to the same
   user.

TROUBLESHOOTING
-------------

* If the popup does not display, check the following:

  - Open your browsers developer console and check the console for errors.
    Submit this information in your issue on d.o.

MAINTAINERS
-----------

Current maintainers:
 * Bryden Arndt (bryden) - https://wwww.drupal.org/u/bryden
